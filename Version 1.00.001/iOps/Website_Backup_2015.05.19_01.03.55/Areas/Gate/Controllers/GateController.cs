﻿using iOps.Website.App_Code;
using System.Web.Mvc;
using iOps.Core.Model;
using iOps.Data;
using System.Linq;
using iOps.Website.Controllers;

namespace iOps.Website.Areas.Gate.Controllers
{
    public class GateController : BaseController
    {
        private iopsContext db_iops = new iopsContext();
        private iopsWeatherEntities db = new iopsWeatherEntities();

        //[CustomAuthorize(Roles = "admin")]

        private string GateLabel(int gateNum)
        {
            string lr = (gateNum % 2 == 0) ? "R" : "L";
            int gn = ((int)System.Math.Floor(((double)gateNum + 1.0) / 2)) + 10;
            return gn.ToString() + lr;
        }

        private void setGateTemps(int gateNum) {
            int screenNumber = 8;
            //int screenNumber = (from us in db_iops.Users
            //                    where us.Username.Contains(User.UserName)
            //                    select us.UsersXrefScreens).Count();

            //int screenNumber =  (from s in db.Screens
            //                           where s.Name.Contains("CID") && !s.Name.Contains("Term")
            //                           select s).Count();

            
            TempData["CIDMaxScreens"] = screenNumber;
            TempData["GateNumber"] = gateNum;
            TempData["GateLabel"] = GateLabel(gateNum);
        }

        [CustomAuthorize]
        public PartialViewResult ShowGates(int gateNum)
        {
            setGateTemps(gateNum);
            return PartialView("_ShowGates");            
        }

        public PartialViewResult ShowServiceCounters(int gateNum)
        {
            setGateTemps(gateNum);
            return PartialView("_ShowServiceCounters");
        }
    }
}