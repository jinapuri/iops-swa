﻿using iOps.Website.App_Code;
using System.Web.Mvc;
using System.Web.UI;
using iOps.Data;
using System.Linq;
using iOps.Website.Controllers;

namespace iOps.Website.Areas.Airport.Controllers
{
    //[CustomAuthorize(Roles = "admin")]
    [CustomAuthorize]
    public class AirportController : BaseController
    {        
        // GET: Airport/ShowAirport
        //[OutputCache(Duration=int.MaxValue, VaryByParam="none",Location=OutputCacheLocation.Server)]
        public PartialViewResult ShowAirport(string clientName, int numberOfScreens)
        {
            switch (clientName)
            {
                case "CID":
                    TempData["CIDMaxScreens"] = numberOfScreens;
                    return PartialView("_ShowCIDAirport");
                case "SLL":
                    TempData["CIDMaxScreens"] = 8;
                    return PartialView("_ShowSLLAirport");
                default:
                    return PartialView();
            }
        }
    }
}