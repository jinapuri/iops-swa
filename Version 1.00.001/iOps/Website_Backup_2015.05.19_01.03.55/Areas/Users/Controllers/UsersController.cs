﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

using iOps.Core.Model;

namespace iOps.Website.Areas.Users.Controllers
{
    public class UsersController : Controller
    {
        private iopsContext db = new iopsContext();

        // GET: Users/Users
        public async Task<ActionResult> Index()
        {
            var users = db.Users.Include(u => u.Salutation);
            return View(await users.ToListAsync());
        }

        // GET: Users/Users/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Users/Create
        public ActionResult Create()
        {
            ViewBag.SalutationID = new SelectList(db.Salutations, "ID", "ControlField");
            return View();
        }

        // POST: Users/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,ControlField,Username,Password,PasswordDateTime,PasswordSalt,Approved,Active,SalutationID,FirstName,LastName,MiddleInitial,JobTitle,Department,HireDate,BirthDate,IsDeleted,ReadyForArchive")] User user)
        {
            if (ModelState.IsValid)
            {
                user.ID = Guid.NewGuid();
                db.Users.Add(user);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.SalutationID = new SelectList(db.Salutations, "ID", "ControlField", user.SalutationID);
            return View(user);
        }

        // GET: Users/Users/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.SalutationID = new SelectList(db.Salutations, "ID", "ControlField", user.SalutationID);
            return View(user);
        }

        // POST: Users/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,ControlField,Username,Password,PasswordDateTime,PasswordSalt,Approved,Active,SalutationID,FirstName,LastName,MiddleInitial,JobTitle,Department,HireDate,BirthDate,IsDeleted,ReadyForArchive")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.SalutationID = new SelectList(db.Salutations, "ID", "ControlField", user.SalutationID);
            return View(user);
        }

        // GET: Users/Users/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Users/Users/Destroy/5
        public async Task<ActionResult> Destroy(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Users/Destroy/5
        [HttpPost, ActionName("Destroy")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DestroyConfirmed(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // UserAdminList_Read
        public ActionResult UserAdminList_Read([DataSourceRequest] DataSourceRequest request)
        {
            IQueryable<iOps.Core.Model.User> users = db.Users.Include(u => u.Salutation);
            DataSourceResult result = users.ToDataSourceResult(request);
            return Json(result);
        }

        // UserAdminList_Destroy
        public ActionResult UserAdminList_Destroy([DataSourceRequest]DataSourceRequest request, User user)
        {
            if (ModelState.IsValid)
            {
                this.Delete(user.ID);
            }
            // Return the removed product. Also return any validation errors.
            return Json(new[] { user }.ToDataSourceResult(request, ModelState));
        }
                
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
