﻿using iOps.Website.App_Code;
using System.Web.Mvc;
using iOps.Website.Controllers;

namespace iOps.Website.Areas.PCA.Controllers
{
    public class PCAController : BaseController
    {
        // GET: PCA/ShowPCA
        //[CustomAuthorize(Roles="admin")]
        [CustomAuthorize]
        public PartialViewResult ShowPCA(int gateNum)
        {
            TempData["GateNumber"] = gateNum;
            return PartialView("_ShowPCA");
        }
    }
}