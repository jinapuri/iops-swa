﻿using iOps.Website.App_Code;
using System.Web.Mvc;
using iOps.Website.Controllers;

namespace iOps.Website.Areas.GPU.Controllers
{
    public class GPUController : BaseController
    {
        // GET: GPU/ShowGPU
        //[CustomAuthorize(Roles = "admin")]
        [CustomAuthorize]
        public PartialViewResult ShowGPU(int gateNum)
        {
            TempData["GateNumber"] = gateNum;
            return PartialView("_ShowGPU");
        }
    }
}