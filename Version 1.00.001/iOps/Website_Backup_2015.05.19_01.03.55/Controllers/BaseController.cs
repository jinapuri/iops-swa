﻿using iOps.Service.Security;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iOps.Website.Controllers
{
    public class BaseController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }
    }
}