﻿$(document).ready(function () {
    $("#splitter").kendoSplitter({
        orientation: "horizontal",
        panes: [
            { resizable: false, scrollable: false, collapsible: true, size: "200px" },
            { resizable: false, scrollable: false, collapsible: false, size: "100%" }
        ],
        expand: function (e) {
            $(".Window-Container").closest(".k-window").width($(".splitter-main").width() - 240);
            $(".Window-Container").closest(".k-window").css({ left: 246 });
        },
        collapse: function (e) {
            $(".Window-Container").closest(".k-window").width($(".splitter-main").width() + 160);
            $(".Window-Container").closest(".k-window").css({ left: 46 });
        }
        //contentLoad: onContentLoad,
        //resize: onResize
    });
    $("#splitter1").kendoSplitter({
        orientation: "horizontal",
        panes: [
            { resizable: true, scrollable: false, collapsible: true, size: "40px" },
            { resizable: false, scrollable: false, collapsible: true, size: "300px" },
            { resizable: false, scrollable: false, collapsible: false, size: "0px" }
        ],
        //expand: onExpand,
        expand: function (e) {
            setTimeout(function () {
                $(e.contentElement).find(".k-splitter").each(function () {
                    $(this).data("kendoSplitter").trigger("resize");
                });
            }, 300);
        }
        //collapse: onCollapse,
        //contentLoad: onContentLoad,
        //resize: onResize
    });

    var slide = kendo.fx($("#slide-in-share")).slideIn("right"),
                visible = true;

    $("#slide-in-handle").click(function (e) {
        if (visible) {
            slide.reverse();
            $("#slide-in-share").css("margin-left", "43px")
            $("#slide-in-share").width("240px");
            $("#slide-in-wrapper").width("240px");
        } else {
            slide.play();
            $("#slide-in-share").css("margin-left", "0px")
            $("#slide-in-share").width("40px");
            $("#slide-in-wrapper").width("60px");
        }
        visible = !visible;
        e.preventDefault();
    });
});