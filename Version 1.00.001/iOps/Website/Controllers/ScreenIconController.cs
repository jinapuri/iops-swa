﻿using System.Web.Mvc;
using iOps.Core.Model;
using iOps.Data;
using System.Linq;

namespace iOps.Website.Controllers
{
    public class ScreenIconController : BaseController
    {
        private iopsContext db_iops = new iopsContext();
        private iopsWeatherEntities db = new iopsWeatherEntities();

        // GET: ScreenIcon
        [HttpGet]
        public PartialViewResult GetScreenIcons(string clientName)
        {
            iopsWeatherEntities db = new iopsWeatherEntities();
            int screenNumber = (from s in db_iops.Screens
                                    where s.Name.Contains(clientName) 
                                       select s).Count();

            TempData["MaxScreens"] = screenNumber;
            TempData["ClientName"] = clientName;
            return PartialView("_ScreenIcons");
        }
        
        public ActionResult WriteScreenAjaxCalls(string clientName, int numberOfScreens)
        {
            string js = string.Empty;
            for (int a = 1; a <= numberOfScreens; a++)
            {
                

                    //Gate 10
                 if (a == 1)
                {
                    js += @"$('.screen-ajax"+a+"').click(function (e) {"+
                    "$.ajax({url: '/Gate/Gate/ShowGates',"+
                    "type: \"GET\","+
                    "contentType: 'text/html',"+
                    "data: {gateNum:10},"+
                    "success: function (data) {"+
                        "CloseKendoWindow();"+
                        "$('#main-body').html(data);"+
                        "ChangeIcons("+(a)+");"+
                    "}});});";
                }

                    //Gate 12
                  else if (a == 2)
                {
                    js += @"$('.screen-ajax"+a+"').click(function (e) {"+
                    "$.ajax({url: '/Gate/Gate/ShowGates',"+
                    "type: \"GET\","+
                    "contentType: 'text/html',"+
                   "data: {gateNum:12},"+
                    "success: function (data) {"+
                        "CloseKendoWindow();"+
                        "$('#main-body').html(data);"+
                        "ChangeIcons("+(a)+");"+
                    "}});});";
                }
            }
            return JavaScript(js);
        }
    }
}