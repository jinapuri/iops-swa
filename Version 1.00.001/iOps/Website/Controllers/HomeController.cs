﻿using System.Web.Mvc;

namespace iOps.Website.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController() 
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

    }
}