﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace iOps.Website.Controllers
{
    public class RDUGatesController : BaseController
    {
        //// GET: RDUGates
        //public ActionResult Index()
        //{
        //    return View("~/Views/RDU/GPU/Index.cshtml");
        //}
        // GET: RDUGates
        public ActionResult LoadGPU()
        {
            return View("~/Views/RDUGPU/Index.cshtml");
        }
        // GET: RDUGates
        public ActionResult LoadPCA()
        {
            return View("~/Views/RDUPCA/Index.cshtml");
        }
    }
}