﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using iOps.Service.Reporting;

namespace iOps.Website.Controllers.Apis
{
    [RoutePrefix("api/v1/reports")]
    public class ReportsController : ApiController
    {
        protected IReportService ReportService { get; private set; }

        public ReportsController(IReportService reportService)
        {
            ReportService = reportService;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
    }
}
